module.exports = function(mysql) {

  // database configuration
  var db = mysql.createPool({
   connectionLimit : 10,
   host     : '192.168.33.3',
   //host     : 'mobilesrv.wisesoft.co.th ',
   database : 'school',
   port     : 3306,
   user     : 'root',
   password : ''
  });

  return {
    query: function(query, data, callback) {
      console.log('query: ' + query);
      console.log(data);
      db.query(query, data, callback);
    },
    execute: function(querys, callback) {

      var current = 0;

      db.getConnection(function(err, connection) {

        if (err) { return callback(false, err); }

        connection.beginTransaction(function(err) {

          if (err) {
            connection.release();
            return callback(false, err);
          }

          // execute
          queryExecute(querys[current]);

        });


        function queryExecute(job) {
          console.log('execute: ' + job.query);
          console.log('data: ');
          console.log(job.data);
          connection.query(job.query, job.data, function(err, result, fields) {
            if (err) {
              connection.rollback(function() {
                connection.release();
                return callback(false, err, current);
              });
            }

            // execute user callback
            try {
              if(job.callback) {
                job.callback(result, fields);
              }
            } catch (err) {
              console.log(err);
            }

            if(current < querys.length-1) {
              // do next job
              queryExecute(querys[++current]);
            } else {
              // do finish
              connection.commit(function(err) {
                // if commit error
                if (err) {
                  connection.rollback(function() {
                    connection.release();
                    return callback(false, err, current);
                  });
                }
                // commit success
                connection.release();
                return callback(true);
              });

            }

          });
        }
      });

    }
  }
}
