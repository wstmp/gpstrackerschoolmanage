var multiparty = require('multiparty');
module.exports = function(req, callback){
  if (req.headers['content-type'] && req.headers['content-type'].indexOf('multipart/') !== -1) {
    //console.log('incoming multipart: content-type: ' + req.headers['content-type']);
    var multipartForm = {};
    var form = new multiparty.Form();// Parts are emitted when parsing the form
    form.on('part', function(part) {
      var bufferArray = [];
      part.on('error', function(err) {
        console.log("Multipart Part Error", err);
        callback(err);
      });
      part.on('data', function(chunk){
        bufferArray.push(chunk);
      });
      part.on('end', function(){
        multipartForm[part.name] = Buffer.concat(bufferArray);
      });
    });
    form.on('close', function() {
      callback(undefined, multipartForm);
    });
    form.on('err', function(err) {
      console.log("Multipart Form Error", err);
      callback(err);
    });
    form.parse(req);
  }else{
    // not a multipart return undefined
    callback();
  }
}
