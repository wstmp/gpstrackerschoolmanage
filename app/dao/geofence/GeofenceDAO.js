module.exports = {
  findBySchoolId: function(schoolId, callback) {

    var geofence = {};

    DBManager.query("select * from geofence where schoolId=?", schoolId, next1);

    function next1(err, result) {
      if(err) {
        return callback(false, err);
      }

      if(result && result.length==0) {
        return callback(true, geofence);
      }

      geofence = result[0];

      // find location points
      DBManager.query("select * from geofencelocationpoints where geoFenceId=?", geofence.geoFenceId, next2);

    }

    function next2(err, result) {
      if(err) {
        return callback(false, err);
      }

      geofence.geofenceLocationPoints = result;

      return callback(true, geofence);
    }
  },
  save: function(userId, schoolId, schoolName, points, callback) {

    // find geofence id
    DBManager.query("select * from geofence where schoolId=?", schoolId, afterSelect);


    function afterSelect(err, result) {
      if(err) {
        return callback(false, err);
      }

      var querys = [];
      var geofence = result[0];

      if(result.length > 0) {
        // update geofence
        querys.push({
          query: "update geofence set ? where schoolId=?",
          data: [{updateBy: userId, updateDate: new Date()}, schoolId]
        });

        // delete all geofence points
        querys.push({
          query: "delete from geofencelocationpoints where geoFenceId=?",
          data: geofence.geoFenceId
        });

        // insert all new geofence points
        if(points && points.length>0) {
            for(var i=0; i<points.length; i++) {
              // set key
              points[i].geoFenceId = geofence.geoFenceId;
              querys.push({
                query: "insert into geofencelocationpoints set ?",
                data: points[i]
              });
            }
        }

      } else {
        // insert new geofence
        querys.push({
          query: "insert into geofence set ?",
          data: {
            schoolId: schoolId,
            locationName: schoolName,
            status: 'Active',
            startTime: '08:00:00',
            endTime: '16:00:00',
            createBy: userId,
            createDate: new Date(),
            updateBy: userId,
            updateDate: new Date()
          },
          callback: afterInsert
        });

        // add insert at runtime
        function afterInsert(result, fields) {
          console.log(result);
          // insert all new geofence points
          if(points && points.length>0) {
              for(var i=0; i<points.length; i++) {
                // set key
                points[i].geoFenceId = result.insertId;
                querys.push({
                  query: "insert into geofencelocationpoints set ?",
                  data: points[i]
                });
              }
          }
        }
      }

      // start execute
      DBManager.execute(querys, function(result, err, index) {
        callback(result, err, index);
      });

    }

  }

}
