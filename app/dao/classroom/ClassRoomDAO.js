module.exports = {
  findBySchoolId: function(schoolId, callback) {

    var classroom;
    var querys = [];

    querys.push({
      query: 'select * from classes where schoolId=? order by classId',
      data: schoolId,
      callback: afterSelectClasses
    });

    function afterSelectClasses(result) {

      classroom = result;

      for(var i=0; i<result.length; i++) {
        var classes = result[i];
        querys.push({
          query: 'select * from room where classId=? order by roomId',
          data: classes.classId,
          callback: afterSelectRoom
        });
      }
    }

    function afterSelectRoom(result) {
      if(result.length > 0) {
        var index, classId = result[0].classId;
        for(index = 0; classId != classroom[index].classId; ++index);
        classroom[index].rooms = result;
      }
    }

    DBManager.execute(querys, function(result, err, index) {
      callback(result, err, classroom);
    });
  },
  save: function(classroom, callback) {

  }
}
