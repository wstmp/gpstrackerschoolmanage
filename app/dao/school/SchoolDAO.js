// school(schoolId, schoolName, address, createDate, updateDate)
module.exports = {
  insert: function(data, callback) {

    DBManager.query("insert into school.school set ?",[data], afterInsert);

    function afterInsert(err, result) {
      if(err) {
        return callback(false, err);
      }

      return callback(true, result);
    }

  },
  update: function(data, id, callback) {
    DBManager.query("update school set ? where schoolId = ?",[data, id], afterUpdate);

    function afterUpdate(err, result) {
      if(err) {
        return callback(false, err);
      }

      return callback(true, result);
    }
  },
  delete: function(id, callback) {
    //var statement = "delete from school where schoolId = ?";
    var statement = "update school set status = 'Deactive' where schoolId = ?";
    DBManager.query(statement,[id], afterUpdate);

    function afterUpdate(err, result) {
      if(err) {
        return callback(false, err);
      }

      return callback(true, result);
    }
  },
  findBySchoolId: function(schoolId, callback) {
    var statement = "select * from school.school where schoolId = ?";

    DBManager.query(statement,schoolId,afterSelect);

    function afterSelect(err, result) {
      if(err) {
        return callback(false, err);
      }

      return callback(true, result);
    }

  },
  findAll: function(callback) {
    var statement = "select *,concat('http://localhost:8081/api/school/getpicture/',school.schoolId,'?',DATE_FORMAT(sysdate(), '%H%i%s')) as pictureUrl from school.school where status = 'Active'";

    DBManager.query(statement,afterSelect);

    function afterSelect(err, result) {
      if(err) {
        return callback(false, err);
      }

      return callback(true, result);
    }
  },
  getUserAdminBySchoolId: function(schoolId, callback) {
    var statement = "select p.firstName,p.lastName " +
                    "from school.person p,school.userLogin ul ,school.teacher t " +
                    "where p.userId = ul.userId " +
                    "and p.personId = t.personId " +
                    "and ul.userRole = 'admin' " +
                    "and t.schoolId = ?";

    DBManager.query(statement,schoolId,afterSelect);

    function afterSelect(err, result) {
      if(err) {
        return callback(false, err);
      }

      return callback(true, result);
    }

  },
  insertUserAdmin: function(data, callback) {
      console.log('insert...');
      console.log(data);

      function parseDate(s) {
        var b = s.split(/\D/);
        return new Date(b[0], --b[1], b[2]);
      }

      var querys = [];

      //table userlogin
      querys.push({
        query: "insert into userlogin set ?",
        data: {
          // userId:idGen,
          userName:data.userName,
          email:data.email,
          password:data.password,
          userRole:'admin',
          status:'A',
          registeredDate: new Date()
        },callback:afterInsertUserLogin
      });


      function afterInsertUserLogin(result,fields){

        //table person
        querys.push({
          query: "insert into person set ?",
          data: {
            // personId:idGen,
            userId: result.insertId,
            // prefixId: idGen,
            firstName: data.firstName,
            //middleName: data.middleName,
            lastName: data.lastName,
            nickName: data.nickName,
            birthDate: parseDate(data.birthDate),
            status: 'A',
            createDate: new Date(),
            createBy: data.createBy,
            updateDate: new Date(),
            updateBy: data.updateBy,
            // profilePicture: 'data.profilePicture',
            // iconMarkerPicture: 'data.iconMarkerPicture'
          },callback:afterInsertPerson
        });

      }
      function afterInsertPerson(result,fields){
        console.log('insertId = '+result.insertId);
        //table teacher
        querys.push({
          query: "insert into teacher set ?",
          data: {
            // teacherId: idGen+1,
            personId: result.insertId,
            schoolId: data.schoolId
          }
        });
      }

      DBManager.execute(querys, function(result, err, index) {
        console.log("result is " + result);
        console.log(err);
        console.log(index);

        callback(result, err, index);
      });

    }
};
