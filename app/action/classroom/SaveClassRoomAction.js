module.exports =  {
  setProperties : function(req, next) {
    return next(null);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {
      return next(true, data);
  },
  success: function(req, res, data) {
    res.json(data);
  },
  failure: function(req, res, data) {
    res.json(data);
  }
};
