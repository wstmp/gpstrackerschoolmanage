module.exports =  {
  setProperties : function(req, next) {

    var data = {};
    
    return next(data);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {

      var dao = daoLookup('/classroom/ClassRoomDAO.js');

      dao.findBySchoolId(data.schoolId, function(result, err, classroom) {
        return next(result, classroom);
      })

  },
  success: function(req, res, data) {
    res.json({result: true, classroom: data});
  },
  failure: function(req, res, data) {
    res.json({result: false});
  }
};
