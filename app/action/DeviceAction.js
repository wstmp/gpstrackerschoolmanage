module.exports =  {

    setProperties : function(req, next) {

      var data = req.body;
      console.log("Data :"+data);

      data.createDate = new Date();
      data.createBy = req.session.userprofile.userId;
      data.updateDate = new Date();
      data.updateBy = req.session.userprofile.userId;

      return next(data);
    },
    validate: function(req, data, next) {

      if("GETALL" == req.config.params.action){
        return next(true, data);
      }

      if(data.deviceId == undefined || data.deviceId == ""){
        data.errmsg = "Device id is not null";
         return next(false, data);
      }

      if(data.brand == undefined || data.brand == ""){
        data.errmsg = "Brand is not null";
         return next(false, data);
      }

      if(data.model == undefined || data.model == ""){
        data.errmsg = "Model is not null";
         return next(false, data);
      }

      if(data.serialNo == undefined || data.serialNo == ""){
        data.errmsg = "Serial no is not null";
         return next(false, data);
      }

      return next(true, data);
    },
    validateFailure: function(req, res, data) {
      res.json(data);
    },
    execute: function(req, data, next) {

        var deviceDAO = daoLookup("/DeviceDAO.js");

        console.log("Action : "+req.config.params.action);
        if("GETALL" == req.config.params.action){
          deviceDAO.findByAll(data, afterActionGetAllDevice);
        }else if("INSERT" == req.config.params.action){
          data.status = "ACTIVE";
          deviceDAO.insert(data, afterActionDevice);
        } else if("UPDATE" == req.config.params.action){
          deviceDAO.update(data,data.deviceId,afterActionDevice);
        }

        function afterActionDevice(result, data) {
            if(data != undefined && data.affectedRows > 0) {
              return next(true, data);
            } else {
              return next(false, data);
            }

        }

        function afterActionGetAllDevice(result, data) {
            if(data != undefined && data.length > 0) {
              return next(true, data);
            } else {
              return next(false, data);
            }

        }
    },
    success: function(req, res, data) {
      if("GETALL" == req.config.params.action){
        res.json(data);
      }else{
        res.json({ result: true});
      }
    },
    failure: function(req, res, data) {
      if("INSERT" == req.config.params.action){
        res.json({ result: false, msg: "Can not insert device"});
      } else if("UPDATE" == req.config.params.action){
        res.json({ result: false, msg: "Can not update device"});
      }
    }
};
