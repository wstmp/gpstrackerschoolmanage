module.exports =  {

    setProperties : function(req, next) {

      var data = {};
      return next(data);
    },
    validate: function(req, data, next) {

      return next(true, data);
    },
    validateFailure: function(req, res, data) {
      res.json(data);
    },
    execute: function(req, data, next) {


        var personDAO = daoLookup("/PersonDAO.js");



        if("INSERT" == req.config.params.action){
          var person = {userId:30, firstName:"test"};
          personDAO.insert(person, afterFindById);
        }else if("UPDATE" == req.config.params.action){
          var person = {firstName:"test update"};
          personDAO.update(person, req.params.personId, afterFindById);
        }else if("DELETE" == req.config.params.action){
          personDAO.delete(req.params.personId, afterFindById);
        }else if("SELECT" == req.config.params.action){
          personDAO.findByUserId(30, afterFindById);
        }

        function afterFindById(err, rows, fields) {
            return next(true, rows);
        }
    },
    success: function(req, res, data) {
      res.json({ result: true, responseMsg: data});
    },
    failure: function(req, res, data) {
      res.json({ result: false, msg: "user or password has invalid"});
    }
};
