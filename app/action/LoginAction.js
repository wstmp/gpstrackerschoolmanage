module.exports =  {

    setProperties : function(req, next) {

      var data = {};
      data.username = req.body.user;
      data.password = req.body.pass;

      return next(data);
    },
    validate: function(req, data, next) {

      if(data.username != undefined && data.username != ""
          && data.password != undefined && data.password != "") {
         return next(true, data);
      }

      data.errmsg = "please input username and password both";

      return next(false, data);
    },
    validateFailure: function(req, res, data) {
      res.json(data);
    },
    execute: function(req, data, next) {

        var userDAO = daoLookup("/UserDAO.js");

        userDAO.loginUserNamePass(data.username, data.password, afterLoginUser);

        function afterLoginUser(err, rows, fields) {

          if(rows != undefined && rows.length == 1) {
            return next(true, rows[0]);
          } else {
            return next(false, rows);
          }

        }
    },
    success: function(req, res, data) {
      req.session.userprofile = data;
      req.session.username = data.userId;
      res.json({ result: true, profile: data});
    },
    failure: function(req, res, data) {
      res.json({ result: false, msg: "user or password has invalid"});
    }
};
