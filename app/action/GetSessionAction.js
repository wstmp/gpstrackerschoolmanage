module.exports =  {
  setProperties : function(req, next) {

    var data = {};
    return next(data);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {
      return next(true, req.session.userprofile);
  },
  success: function(req, res, data) {
    res.json(data);
  },
  failure: function(req, res, data) {
    res.json(data);
  }
};
