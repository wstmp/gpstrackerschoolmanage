module.exports =  {
  setProperties : function(req, next) {

    var data = req.body;

    console.log(data);

    return next(data);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {
    var schoolDAO = daoLookup("/SchoolDAO.js");
    console.log('ACTION:'+req.config.params.action);

    if(req.config.params.action == 'FINDBYID'){
      schoolDAO.findBySchoolId(data.schoolId,afterFindById);

    }else if(req.config.params.action == 'UPDATE'){
      delete data.createDate;
      data.updateDate = new Date();
      schoolDAO.update(data, data.schoolId, afterUpdateSchool);

    }else{

    }

    function afterFindById(err, rows, fields) {

      if(rows != undefined && rows.length == 1) {
        return next(true, rows[0]);
      } else {
        return next(false, rows);
      }

    }

    function afterUpdateSchool(err, rows, fields) {
      console.log(rows);
      if(rows != undefined && rows.affectedRows > 0) {
        console.log(rows);
        return next(true, rows);
      } else {
        return next(false, rows);
      }

    }

  },
  success: function(req, res, data) {
    if(req.config.params.action == 'FINDBYID'){
      res.json(data);
    }else if(req.config.params.action == 'UPDATE'){
      res.json({result:true});
    }
  },
  failure: function(req, res, data) {
    if(req.config.params.action == 'FINDBYID'){
      res.json(data);
    }else if(req.config.params.action == 'UPDATE'){
      res.json({result:false, msg: "Can not update school"});
    }
  }
};
