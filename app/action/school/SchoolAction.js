module.exports =  {
  setProperties : function(req, next) {

    var data = {};
    console.log("-- SchoolAction --");

    multiparty(req, function(err, multipart){
      var schoolData = JSON.parse(multipart['json'].toString('utf8'));
      data = schoolData;

      var schoolPicture = multipart['schoolPicture'];

      if(schoolPicture == 'undefined' || schoolPicture == null || schoolPicture == ""){
          schoolPicture = null;
      }

      data.picture = schoolPicture;
      console.log(data);
      return next(data);
    });
    //return next(data);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {
    var schoolDAO = daoLookup("/school/SchoolDAO.js");
    console.log('ACTION:'+req.config.params.action);

    if(req.config.params.action == 'INSERT'){
      data.status = 'Active';
      data.createDate = new Date();
      delete data.pictureUrl;
      schoolDAO.insert(data,afterActionSchool);

    }else if(req.config.params.action == 'UPDATE'){
      // delete data.createDate;
      data.updateDate = new Date();
      delete data.pictureUrl;
      schoolDAO.update(data, data.schoolId, afterActionSchool);

    }else if(req.config.params.action == 'DELETE'){
      schoolDAO.delete(data.schoolId, afterActionSchool);
    }

    function afterActionSchool(result,rows) {
        return next(result, rows);
    }

  },
  success: function(req, res, data) {
    if(req.config.params.action == 'INSERT'){
      res.json({result:true});
    }else if(req.config.params.action == 'UPDATE'){
      res.json({result:true});
    }else if(req.config.params.action == 'DELETE'){
      res.json({result:true});
    }
  },
  failure: function(req, res, data) {
    if(req.config.params.action == 'INSERT'){
      res.json({result:false, msg: data});
    }else if(req.config.params.action == 'UPDATE'){
      res.json({result:false, msg: data});
    }else if(req.config.params.action == 'DELETE'){
      res.json({result:false, msg: data});
    }
  }
};
