module.exports =  {
  setProperties : function(req, next) {

    console.log("School Id ->"+req.params.schoolId);
    var data = {};
    data.schoolId = req.params.schoolId;

    return next(data);
  },
  validate: function(req, data, next) {
      if(data.schoolId == undefined || data.schoolId == ""){
        data.errmsg = "School id is not null";
         return next(false, data);
      }
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {
    var schoolDAO = daoLookup("/school/SchoolDAO.js");

    schoolDAO.findBySchoolId(data.schoolId,afterSclectSchool);

    function afterSclectSchool(result,rows) {
        return next(result, rows);
    }

  },
  success: function(req, res, data) {
      console.log("data.picture");
      if(data[0].picture == null || data[0].picture == ""){
        res.json({result:false, msg: "Picture no found."});
      }else{
        // console.log(data);
        // console.log(data[0].picture);
        res.writeHead(200, {"Content-Type": "image/png"});
        res.end(data[0].picture);
      }
  },
  failure: function(req, res, data) {
      res.json({result:false, msg: data});
  }
};
