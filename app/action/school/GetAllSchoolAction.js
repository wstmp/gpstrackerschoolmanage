module.exports =  {
  setProperties : function(req, next) {

    var data = {};

    return next(data);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {

      var dao = daoLookup('/school/SchoolDAO.js');

      dao.findAll(afterFindAll);

      function afterFindAll(result, data) {
          return next(true, data);
      }
  },
  success: function(req, res, data) {
    res.json(data);
  },
  failure: function(req, res, data) {
    res.json({result: false});
  }
};
