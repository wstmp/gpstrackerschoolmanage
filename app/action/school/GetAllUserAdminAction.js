module.exports =  {
  setProperties : function(req, next) {

    var data = {};
    data = req.body;

    return next(data);
  },
  validate: function(req, data, next) {
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {

      var schoolDAO = daoLookup("/school/SchoolDAO.js");

      schoolDAO.getUserAdminBySchoolId(data.schoolId,afterActionSchool);

      function afterActionSchool(result,rows) {
          return next(result, rows);
      }

  },
  success: function(req, res, data) {
    res.json(data);
  },
  failure: function(req, res, data) {
    res.json({result: false});
  }
};
