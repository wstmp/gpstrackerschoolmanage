module.exports =  {
  setProperties : function(req, next) {

    var data = {};
    data.schoolId = req.body.schoolId;

    return next(data);
  },
  validate: function(req, data, next) {
    console.log("> validate schoolId = " + data.schoolId);

    if(data.schoolId == undefined || data.schoolId == "") {
        data.errmsg = "please input school id";
        return next(false, data);
    }

    return next(true, data);

  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {

    // get geofence from school id
    var geofenceDAO = daoLookup('/geofence/GeofenceDAO.js');

    var schoolId = data.schoolId;
    console.log("> get geofence of schoolId = " + schoolId);

    console.log(req.session.userprofile);

    geofenceDAO.findBySchoolId(schoolId, afterFindBySchoolId);

    function afterFindBySchoolId(result, data) {
      return next(result, data);
    }

  },
  success: function(req, res, data) {
    res.json(data);
  },
  failure: function(req, res, data) {
    res.json(data);
  }
};
