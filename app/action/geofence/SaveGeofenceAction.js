module.exports =  {
  setProperties : function(req, next) {

    var data = req.body;
    // data.geofence = req.body.geofence;
    // var geofence = req.body.geofence;
    // var geofence = req.body.geofence;
    console.log('> SaveGeofenceAction');

    return next(data);
  },
  validate: function(req, data, next) {
    if(data.schoolId == undefined || data.schoolId == ""){
      data.errmsg = "School id is not null";
       return next(false, data);
    }
    return next(true, data);
  },
  validateFailure: function(req, res, data) {
    res.json(data);
  },
  execute: function(req, data, next) {

    // get geofence from school id
    var geofenceDAO = daoLookup('/geofence/GeofenceDAO.js');

    var schoolId = data.schoolId;
    var userId = req.session.userprofile.userId;
    var schoolName = req.session.userprofile.schoolName;
    console.log("> get geofence of schoolId = " + schoolId);

    // prepare geofence points
    var points = data.geofence;
    console.log(points);

    // execute dao
    geofenceDAO.save(userId, schoolId, schoolName, points, function(result, err, index) {
      console.log(result);
      if(err) {
        console.log(err);
        console.log('error at '+index);
      }

      next(result, err);
    });

  },
  success: function(req, res, data) {
    console.log('success');
    res.json({result: true});
  },
  failure: function(req, res, data) {
    console.log('error');
    res.json({result: false, responseMsg: data});
  }
};
