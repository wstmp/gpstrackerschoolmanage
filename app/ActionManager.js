module.exports = function(ctrl, requiredSession, params) {

  return function(req, res) {

    try {

      var controller = require(ctrl);

      /**********************************************
       *          check session authorized          *
       **********************************************/
      if(requiredSession){
        console.log('user login: '+req.session.username);
        var username = req.session.username;
        if(username == undefined || username == null || username == ""){
            res.json({ result: false, responseCode: 401, responseMsg: 'Unauthorized'});
            return;
        }
      }

      // set config param to require object
      if(params != undefined) {
        req.config = {};
        req.config.params = params;
      }

      controller.setProperties(req ,afterSetProperties);

      function afterSetProperties(data) {
        controller.validate(req, data, afterValidate);
      }

      function afterValidate(result, data) {
        if(result) {
          controller.execute(req, data, afterExecute);
        } else {
          controller.validateFailure(req, res, data);
        }
      }

      function afterExecute(result, data) {
        if(result) {
          controller.success(req, res, data);
        } else {
          controller.failure(req, res, data);
        }
      }

    } catch(err) {
      res.json({result: false, responseMsg: err});
    }

  };

};
