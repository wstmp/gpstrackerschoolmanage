module.exports = [
    {method: 'post', url: '/login', callback: '/LoginAction.js', requiredSession: false, params: {action: 'LOGIN'} },
    {method: 'get', url: '/school/all', callback: '/school/GetAllSchoolAction.js'},
    {method: 'post', url: '/school/insert', callback: '/school/SchoolAction.js',params: {action: 'INSERT'}},
    {method: 'post', url: '/school/update', callback: '/school/SchoolAction.js',params: {action: 'UPDATE'}},
    {method: 'post', url: '/school/delete', callback: '/school/SchoolAction.js',params: {action: 'DELETE'}},
    {method: 'get', url: '/school/getpicture/:schoolId', requiredSession: false,callback: '/school/SchoolGetPictureAction.js'},
    {method: 'post', url: '/school/useradmin/all', callback: '/school/GetAllUserAdminAction.js'},
    {method: 'post', url: '/geofence', callback: '/geofence/GetGeofenceAction.js'},
    {method: 'post', url: '/geofence/save', callback: '/geofence/SaveGeofenceAction.js'},
    {method: 'post', url: '/classroom', callback: '/classroom/SaveClassRoomAction.js'}
];
