angular.module('trackermngApp' ,
  ['ngRoute','ngAnimate','ngMaterial','app.controllers','app.services','app.directives'])
.config(['$routeProvider',
function($routeProvider) {
  $routeProvider.
    when('/:schoolId/login', {
      templateUrl: 'html/login.html',
      controller: 'LoginCtrl'
    }).
    when('/menu', {
      templateUrl: 'html/menu.html',
      controller: 'MenuCtrl'
    }).
    otherwise({
      redirectTo: '/1/login'
    });
}]);
