angular.module('ctrl.login',[])
.controller('LoginCtrl', function($scope,$location,$http,$routeParams) {
  console.log('LoginCtrl...');

  $scope.login = function(user){
    console.log('do login..');
    $http.post('/api/login' , { user: user.name, pass: user.password } )
    .success(function(data) {
      console.log(data);
      if(data.result){
        sessionStorage.setItem('userprofile', JSON.stringify(data));
        $location.path('/menu');
      }else{
        $scope.msg = data.msg;
      }
    })
    .error(function(data) {
      console.log('Error: ' + data);
    });
  }
})

.controller('CompleteCtrl', function($scope,$http,$location) {
  // alert('CompleteCtrl...');
  console.log('CompleteCtrl...');

  $scope.obj = JSON.stringify(sessionStorage.getItem('userprofile'));

  $scope.logout = function(){


    $http.get('/student/logout')
    .success(function(data) {
      console.log(data);
      // $location.path("/login");
    })
    .error(function(data) {
      console.log('Error: ' + data);
    });
  }
  $scope.student = function(){
    $location.path("/student");
  }

  $scope.clear = function() {
    $scope.obj = undefined;
  }


  $scope.getSession = function() {
    $http.get('/api/getSession')
    .success(function(data) {
      $scope.obj = data;
      console.log(data);
      // $scope.$apply();
      // $location.path("/login");
    })
    .error(function(data) {
      console.log('Error: ' + data);
    });
  }

});
