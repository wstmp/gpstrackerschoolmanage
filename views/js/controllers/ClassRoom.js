angular.module('ctrl.classroom',[])
.controller('ClassRoomCtrl', function($scope, $rootScope, ClassRoom) {

  // set title
  $rootScope.title = "Class/Room Management";

  console.log('ClassRoomCtrl start');
  ClassRoom.get(success, error);

  function success(data) {
    console.log('-- result --');
    console.log(data);
    $scope.classroom = data.classroom;
  }

  function error(err) {
    console.log(err);
  }

});
