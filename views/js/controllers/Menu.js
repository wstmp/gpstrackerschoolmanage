angular.module('ctrl.menu',[])
.controller('MenuCtrl', function($scope,$rootScope,$location,$http,$mdSidenav) {
  $scope.toggleLeft = function() {
    $mdSidenav('left').toggle();
  };

  $scope.pageClass = 'slideUp';
  $scope.pageContentClass = 'slideRight';

  $scope.templates =
      [ { name: 'template1.html', url: 'html/school/schoolList.html'},
        { name: 'geofence.html', url: 'html/geofence.html'},
        { name: 'schoolAdmin.html', url: 'html/school/schoolUserAdminList.html'}
      ];

    $scope.template = $scope.templates[0];

  $scope.goto = function(index){
    $rootScope.title = '';
    $scope.subPageClass = 'slideRight';
    $scope.template = $scope.templates[index];
    $mdSidenav('left').close();
  }

});
