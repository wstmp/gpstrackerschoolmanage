angular.module('ctrl.geofence',[])
.controller('GeofenceCtrl', function($scope, $rootScope, Geofence,SchoolService) {

  // set title
  $rootScope.title = "Geo-Fence Management";

  var poly, map;
  var markers = [];
  var path = new google.maps.MVCArray;
  var geofence;

  $scope.isEditMode = false;
  $scope.mySchool = undefined;


  initializeSchoolOptions(initialize);


  /*
   * $scope params
   */

  $scope.edit = function() {
    editMode();
  };

  $scope.save = function() {
    viewMode();
    saveMap();
  };

  $scope.cancel = function() {
    $scope.isEditMode = false;
    clearMap();
    initialize();
  };

  /*
   * function
   */

   function initialize() {
     var defaultPosition = new google.maps.LatLng(13.7246005, 100.6331108); // Bangkok
     clearMap();
     // create maps
     map = new google.maps.Map(document.getElementById("map"), {
       zoom: 11,
       center: defaultPosition,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     });

    // alert(document.getElementById("map").innerHTML);
     // create polygon
     poly = new google.maps.Polygon({
       strokeWeight: 3,
       fillColor: '#5555FF'
     });
     poly.setMap(map);
     poly.setPaths(new google.maps.MVCArray([path]));

     getGeofenceFromBackendService();

   }

   function addPoint(event) {
     console.log(event);

     path.insertAt(path.length, event.latLng);

     var marker = new google.maps.Marker({
       position: event.latLng,
       map: map,
       draggable: true
     });
     markers.push(marker);
     marker.setTitle("#" + path.length);

     if($scope.isEditMode) {
       addMarkerEvent(marker);
     }
   }

   function addMarkerEvent(marker) {
     google.maps.event.addListener(marker, 'click', function() {
       marker.setMap(null);
       for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
       markers.splice(i, 1);
       path.removeAt(i);
       }
     );

     google.maps.event.addListener(marker, 'dragend', function() {
       for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
       path.setAt(i, marker.getPosition());
       }
     );
   }

   function removeMarkerEvent(marker) {
     google.maps.event.clearListeners(marker, 'click');
     google.maps.event.clearListeners(marker, 'dragend');
   }

   function editMode() {

     $scope.isEditMode = true;

     markersBeforeEdit = markers;

     google.maps.event.addListener(map, 'click', addPoint);

     for(var i=0; i<markers.length; i++) {
       addMarkerEvent(markers[i]);
     }
   }

   function viewMode() {

     $scope.isEditMode = false;

     google.maps.event.clearListeners(map, 'click');

     for(var i=0; i<markers.length; i++) {
       removeMarkerEvent(markers[i]);
     }
   }

   function saveMap() {

      var geofenceLocationPoints = [];

      for(var i=0; i<markers.length; i++) {
        var point = {};
        point.latitude = markers[i].position.k;
        point.longitude = markers[i].position.D;

        geofenceLocationPoints.push(point);
      }

      console.log('> save geofence');
      console.log(geofenceLocationPoints);
      Geofence.save(geofenceLocationPoints,$scope.mySchool.schoolId);

   }

   function clearMap() {

     for(var i=0; i<markers.length; i++) {
       markers[i].setMap(null);
       path.removeAt(i);
     }

     poly = undefined;
     map = undefined;
     markers = [];
     path = new google.maps.MVCArray;
     geofence = undefined;

   }

   function getGeofenceFromBackendService() {
     console.log('> start get geofence from backend service....');
     Geofence.get($scope.mySchool.schoolId,success, error);

     function success(data) {
       geofence = data;
       markOnMap();
     }

     function error(data) {
       console.log(data);
     }

   }

   function markOnMap() {
     console.log(geofence);

     if(geofence.geofenceLocationPoints != undefined) {
       var locations = geofence.geofenceLocationPoints;
       for(var i=0; i<locations.length; i++) {
         var latLng = new google.maps.LatLng(locations[i].latitude, locations[i].longitude, true);
         addPoint({latLng: latLng});

         if(i==0) {
           // set center of map near geofence
           map.setCenter(latLng);
           map.setZoom(15);
         }
       }
     }

   }


   function initializeSchoolOptions(callNext) {
     //$scope.schoolOptions = undefined;
     SchoolService.getAllSchool(success, error);

     function success(data) {
       $scope.schoolOptions = data;
       $scope.mySchool = $scope.schoolOptions[0];
       return callNext();
     }

     function error(data) {
       console.log(data);
     }
   }

  $scope.changeSchoolOption = function() {
     initialize();
  }

});
