angular.module('ctrl.school',[])
.controller('SchoolCtrl', function($scope,$location,$http) {
  $scope.obj = JSON.parse(sessionStorage.getItem('schoolinfo'));
  $scope.isShow = false;
  $scope.$watchGroup(['obj.schoolName','obj.address'], function(newValues, oldValues, scope) {
    console.log($scope.obj);

    var c = 0;
    for(var i=0;i<oldValues.length;i++){
      if(newValues[i] != oldValues[i]){
        c++;
      }
    }
    if(c > 0){
      $scope.isShow = true;

      $scope.isEdit = false;
      $scope.isBtnEdit = false;
      $scope.isBtnClose = false;

    }else{
      $scope.isShow = false;
    }

   });

   function disabled(){
     $scope.isEdit = false;
     $scope.isBtnEdit = false;
     $scope.isBtnClose = true;
   }
   function enabled(){
     $scope.isBtnClose = false;
     $scope.isBtnEdit = true;
     $scope.isEdit = true;
   }

   $scope.isEdit = true;
   $scope.isBtnEdit = true;
  $scope.changeToEdit = function(state){

    if(state == 2){
      enabled();
    }else if(state == 1){
      disabled();
    }
  }

  $scope.save = function(obj){
    // var schoolDAO =
    $http.post('/api/school/update' , obj )
    .success(function(data) {
      console.log(data);
      // $scope.obj = data;
      if(data.result){
        enabled();
      }

    })
    .error(function(data) {
      console.log('Error: ' + data);
    });
  }

})

.controller('SchoolListCtrl', function($scope,$http,$mdDialog,SchoolService) {
  // alert('CompleteCtrl...');
    console.log('...SchoolListCtrl...');

    $scope.schoolAlls = undefined;
    initializeSchoolList();

    function initializeSchoolList() {
      //$scope.schoolOptions = undefined;
      SchoolService.getAllSchool(success, error);

      function success(data) {
        console.log(data);
        $scope.schoolAlls = data;
      }

      function error(data) {
        console.log(data);
      }
    }

    //Add school
    $scope.doAddSchool = function(school,rowIndex) {
      $mdDialog.show({
        templateUrl: 'html/school/addAndEditSchool.html',
        controller: addDialogController
      }).then(function(saveAdd) {
        $scope.alert = 'You said "Okay".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    function addDialogController($scope, $mdDialog) {
      console.log('addDialogController');

      $scope.obj = {};
      $scope.fromAction = 'Add';

      $scope.saveAdd = function(objAdd) {
        console.log('saveAdd');
        console.log(objAdd);

        var fd = new FormData();
        fd.append('json', JSON.stringify(objAdd));
        fd.append('schoolPicture', $scope.schoolPicture);


        SchoolService.insertSchool(fd,success, error);
        function success(data) {
          $mdDialog.hide();
          initializeSchoolList();
        }

        function error(data) {
          console.log(data);
        }

      };
    }


    //Edit School
    $scope.doEditSchool = function(school,rowIndex) {
      $mdDialog.show({
        templateUrl: 'html/school/addAndEditSchool.html',
        controller: editDialogController,
        locals: {school: school,rowIndex: rowIndex,schoolAlls:$scope.schoolAlls}
      }).then(function(saveEdit) {
        //$scope.timeStr = new Date().getTime();
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    function editDialogController($scope, $mdDialog,school,rowIndex,schoolAlls) {
      console.log('eidtDialogController');
      console.log("rowIndex :"+rowIndex);
      console.log(school.schoolName);
      $scope.obj = {};
      $scope.obj.schoolId = school.schoolId;
      $scope.obj.schoolName = school.schoolName;
      $scope.obj.address = school.address;
      $scope.obj.pictureUrl = school.pictureUrl;
      $scope.fromAction = 'Edit';

      $scope.saveEdit = function(objEdit) {
        console.log('Edit');
        console.log(objEdit);

        var fd = new FormData();
        fd.append('json', JSON.stringify(objEdit));
        fd.append('schoolPicture', $scope.schoolPicture);

        SchoolService.updateSchool(fd,success, error);
        function success(data) {
          $mdDialog.hide();

          schoolAlls[rowIndex] = objEdit;
          schoolAlls[rowIndex].pictureUrl = schoolAlls[rowIndex].pictureUrl + "a";
        }

        function error(data) {
          console.log(data);
        }

      };
    }

    $scope.showConfirmDelete = function(rowIndex,school) {
    var confirm = $mdDialog.confirm()
      .title('Are you sure to delete?')
      .content('Delete school')
      .ok(' OK ')
      .cancel('Cancel');
    $mdDialog.show(confirm).then(function() {
      SchoolService.deleteSchool(school,success, error);
      function success(data) {
        $scope.schoolAlls.splice(rowIndex, 1);
      }

      function error(data) {
        console.log(data);
      }

    }, function() {
      $scope.alert = 'You decided to keep your debt.';
    });
  };
})

.controller('SchoolUserAdminCtrl', function($scope,$mdDialog,$http,SchoolService) {
  SchoolService.getAllSchool(success, error);

  function success(data) {
    $scope.schoolOptions = data;
    $scope.mySchool = $scope.schoolOptions[0];
    return initializeUserAdminList();
  }

  function error(data) {
    console.log(data);
  }

  $scope.changeSchoolOption = function() {
     initializeUserAdminList();
  }

  function initializeUserAdminList() {
    SchoolService.getSchoolUserAdminList($scope.mySchool.schoolId,success, error);

    function success(data) {
      console.log(data);
      $scope.userAdminList = data;
    }

    function error(data) {
      console.log(data);
    }(success, error);
  }

  //Add school
  $scope.doAddUserAdminSchool = function(school,rowIndex) {
    $mdDialog.show({
      templateUrl: 'html/school/addAndEditUserAdminSchool.html',
      controller: addUserAdminDialogController
    }).then(function(saveAdd) {
      $scope.alert = 'You said "Okay".';
    }, function() {
      $scope.alert = 'You cancelled the dialog.';
    });
  };


  function addUserAdminDialogController($scope, $mdDialog) {
    console.log('addUserAdminDialogController');

    $scope.obj = {};
    $scope.fromAction = 'Add';

    $scope.saveAdd = function(objAdd) {
      console.log('saveAdd');
      console.log(objAdd);

      var fd = new FormData();
      fd.append('json', JSON.stringify(objAdd));
      fd.append('schoolPicture', $scope.schoolPicture);


      SchoolService.insertSchool(fd,success, error);
      function success(data) {
        $mdDialog.hide();
        initializeSchoolList();
      }

      function error(data) {
        console.log(data);
      }

    };
  }

});
