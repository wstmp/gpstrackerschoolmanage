angular.module('ctrl.device',[])
.controller('DeviceAddAndUpdateCtrl', function($scope,$mdDialog,$http,$routeParams,actionFrom) {
  console.log('DeviceAddAndUpdateCtrl ...');

  console.log(actionFrom);
  $scope.obj = undefined;
  var deviceAction = actionFrom;

  console.log(deviceAction);
  $scope.obj = {deviceId:'test'};

  if(deviceAction == undefined || deviceAction == null){//Add Device
    console.log('Add action ...');
    $scope.deviceAction = "ADD";
  }else if("EDIT" == deviceAction){// Edit device
    $scope.deviceAction = "EDIT";
    var deviceId = $routeParams.deviceId;

    $http.post('/api/device/getDeviceByDeviceId' , { deviceId:deviceId } )
    .success(function(data) {
      console.log(data);
      $scope.obj = data;
    })
    .error(function(data) {
      console.log('Error: ' + data);
    });

  }

  $scope.save = function(obj){
    console.log(obj);
    $http.post('/api/device/save' ,obj )
    .success(function(data) {
      console.log(data);
      if(data.result){
        console.log(data.result);
      }else{
        $scope.msg = data.msg;
      }
    })
    .error(function(data) {
      console.log('Error: ' + data);
    });
  }


    $scope.saveEdit = function(obj){
      console.log(obj);
      $http.post('/api/device/edit' ,obj )
      .success(function(data) {
        console.log(data);
        if(data.result){
          console.log(data.result);
        }else{
          $scope.msg = data.msg;
        }
      })
      .error(function(data) {
        console.log('Error: ' + data);
      });
    }

})

.controller('DeviceListCtrl', function($scope,$http,$mdDialog,DeviceService) {
  // alert('CompleteCtrl...');
    console.log('DeviceListCtrl...');
    //$scope.selectedAll = false;

    getAddSetScopeAllDevice();
    $scope.isDeleteDevice = false;

    $scope.checkAll = function () {

        //if($scope.selectedAll == undefined) $scope.selectedAll = false;
        $scope.isDeleteDevice = true;

        if ($scope.selectedAll && $scope.selectedAll != undefined) {
            $scope.selectedAll = true;
            $scope.isDeleteDevice = false;
        } else {
            $scope.selectedAll = false;
            $scope.isDeleteDevice = true;
        }


        angular.forEach($scope.deviceAlls, function (item) {
            if ($scope.selectedAll) {
                item.Selected = false;
            } else {
                item.Selected = true;
            }
        });

    };

    $scope.checkToDelete = function () {

        var count = 0;


        for(i=0;i<$scope.deviceAlls.length;i++){
          if ($scope.deviceAlls[i].Selected && $scope.deviceAlls[i].Selected != undefined) {
            count += 1;
            console.log(count);
          }
        }

        console.log("device selected count ->"+count);

        // angular.forEach($scope.deviceAlls, function (item) {
        //     if (item.Selected && item.Selected != undefined) {
        //       count += 1;
        //     }
        // });
        //
        // $q.all($scope.deviceAlls).then(function(){
        //     // This callback function will be called when all the promises are resolved.    (when all the albums are retrived)
        //     console.log("device selected count ->"+count);
        // })
        // .then (function (item) {
        //   console.log("device selected count ->"+count);
        // });
    };



    $scope.doAddDevice = function() {
      //$rootScope.action = 'Add';
      $mdDialog.show({
        templateUrl: 'html/device/addAndEditDevice.html',
        locals: {actionFrom: 'Add'},
        controller: addDialogController
      }).then(function(save) {
        getAddSetScopeAllDevice();
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };


    $scope.doEditDevice = function(device) {
      $mdDialog.show({
        templateUrl: 'html/device/addAndEditDevice.html',
        controller: editDialogController,
        locals: {device: device}
      }).then(function(saveEdit) {
        $scope.alert = 'You said "Okay".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };


    function addDialogController($scope, $mdDialog) {
      console.log('addDialogController');
      $scope.fromAction = 'Add';

      $scope.save = function(device) {
        console.log('saveeeeeeeeeee');
        console.log(device);

        DeviceService.saveDevice(device,success, error);
        function success(data) {
          console.log("Save result :"+data);
          $mdDialog.hide();
        }

        function error(data) {
          console.log(data);
        }


      };

    }

    function editDialogController($scope, $mdDialog,device) {
      console.log('eidtDialogController');
      console.log(device);
      $scope.obj = device;
      $scope.fromAction = 'Edit';

      $scope.saveEdit = function() {
        console.log('Edit');
        console.log(device);

        DeviceService.updateDevice(device,success, error);
        function success(data) {
          console.log("Save result :"+data);
          $mdDialog.hide();
        }

        function error(data) {
          console.log(data);
        }

      };
    }


    function getAddSetScopeAllDevice() {
      $scope.deviceAlls = undefined;
      DeviceService.getAllDevice(success, error);

      function success(data) {
        console.log(data);
        $scope.deviceAlls = data;
      }

      function error(data) {
        console.log(data);
      }

    }

});
