angular.module('service.School', [])
.factory('SchoolService', function($http) {

  return {
    getAllSchool: function(success, error) {
      $http.get('/api/school/all')
        .success(function(data, status, headers, config) {
          success(data);
        })
        .error(function(data, status, headers, config) {
          error(data);
        });
    },
    insertSchool: function(school,success, error) {
        $http.post("/api/school/insert", school, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        }).success(
          function(data, status, headers, config){
            if(data.result){
              console.log(data.result);
              return success(data.result);
            }else{
              return error(data.msg);
            }
          }
        )
        .error(
          function(data, status, headers, config){
            console.log('Error: ' + data);
          }
        );

        // $http.post('/api/school/insert' ,school )
        //   .success(function(data, status, headers, config) {
        //     if(data.result){
        //       console.log(data.result);
        //       return success(data.result);
        //     }else{
        //       return error(data.msg);
        //     }
        //   })
        //   .error(function(data, status, headers, config) {
        //     console.log('Error: ' + data);
        //   });
      },
    updateSchool: function(school,success, error) {

        $http.post("/api/school/update", school, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        }).success(
          function(data, status, headers, config){
            if(data.result){
              console.log(data.result);
              return success(data.result);
            }else{
              return error(data.msg);
            }
          }
        )
        .error(
          function(data, status, headers, config){
            console.log('Error: ' + data);
          }
        );

        // $http.post('/api/school/update' ,school )
        //   .success(function(data, status, headers, config) {
        //     if(data.result){
        //       console.log(data.result);
        //       return success(data.result);
        //     }else{
        //       return error(data.msg);
        //     }
        //   })
        //   .error(function(data, status, headers, config) {
        //     console.log('Error: ' + data);
        //   });
      },
      deleteSchool: function(school,success, error) {
          $http.post('/api/school/delete' ,school )
            .success(function(data, status, headers, config) {
              if(data.result){
                console.log(data.result);
                return success(data.result);
              }else{
                return error(data.msg);
              }
            })
            .error(function(data, status, headers, config) {
              console.log('Error: ' + data);
            });
        },
      getSchoolUserAdminList: function(schoolId,success, error) {
        console.log("-- serivce : getSchoolUserAdminList schoolId ->"+schoolId);

        $http.post('/api/school/useradmin/all',{schoolId:schoolId})
          .success(function(data, status, headers, config) {
            success(data);
          })
          .error(function(data, status, headers, config) {
            error(data);
          });

      }
    }

});
