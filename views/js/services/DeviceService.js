angular.module('service.Device', [])
.factory('DeviceService', function($http) {

  return {
    getAllDevice: function(success, error) {
      $http.get('/api/device/all')
        .success(function(data, status, headers, config) {
          success(data);
        })
        .error(function(data, status, headers, config) {
          error(data);
        });
    },
    saveDevice: function(newDevice,success, error) {

      $http.post('/api/device/save' ,newDevice )
      .success(function(data, status, headers, config) {
        if(data.result){
          console.log("DeviceService : "+data.result);
          return success(data.result);
        }else{
          return error(data.msg);
        }
      })
      .error(function(data) {
        console.log('Error: ' + data);
      });
    },
    updateDevice: function(device,success, error) {

        $http.post('/api/device/edit' ,device )
        .success(function(data, status, headers, config) {
          if(data.result){
            console.log(data.result);
            return success(data.result);
          }else{
            return error(data.msg);
          }
        })
        .error(function(data, status, headers, config) {
          console.log('Error: ' + data);
        });
    }
  }

});
