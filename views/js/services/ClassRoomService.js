angular.module('service.classroom', [])

.factory('ClassRoom', function($http) {

  return {
    get: function(success, error) {
      $http.get('/api/classroom')
        .success(function(data, status, headers, config) {
          success(data);
        })
        .error(function(data, status, headers, config) {
          error(data);
        });
    },
    save: function(classroom) {
      $http.post('/api/classroom', {classroom: classroom})
        .success(function(data, status, headers, config) {
          console.log(status);
        })
        .error(function(data, status, headers, config) {
          console.log(status);
        });
    }
  }
});
