angular.module('service.Geofence', [])

.factory('Geofence', function($http) {

  return {
    get: function(schoolId,success, error) {
      $http.post('/api/geofence',{schoolId:schoolId})
        .success(function(data, status, headers, config) {
          success(data);
        })
        .error(function(data, status, headers, config) {
          error(data);
        });
    },
    save: function(geofence,schoolId) {
      $http.post('/api/geofence/save', {geofence: geofence,schoolId:schoolId})
        .success(function(data, status, headers, config) {
          console.log(status);
        })
        .error(function(data, status, headers, config) {
          console.log(status);
        });
    }
  }
});
