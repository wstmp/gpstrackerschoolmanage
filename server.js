// set up ======================================================================
// var express  = require('express');

var express = require('express'),
    // routes  = require('./routes'),
    http    = require('http'),
    path    = require('path');


var app      = express(); 								// create our app w/ express
var mongoose = require('mongoose'); 					// mongoose for mongodb
var port  	 = process.env.PORT || 8081; 				// set the port
var database = require('./config/database'); 			// load the database config

var morgan = require('morgan'); 		// log requests to the console (express4)
var bodyParser = require('body-parser'); 	// pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var session = require('express-session');
var multiparty = require('multiparty');   //init multiparty

// configuration ===============================================================
mongoose.connect(database.url); 	// connect to mongoDB database on modulus.io

app.use(express.static(__dirname + '/views')); 				// set the static files location /public/img will be /img for users
app.use(morgan('dev')); 										// log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'})); 			// parse application/x-www-form-urlencoded
app.use(bodyParser.json()); 									// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// app.use(session({ name:'12345',secret: 'keyboard cat', cookie: { maxAge: 1000 },resave: true,saveUninitialized:true}));

app.use(session({
  resave: false, // don't save session if unmodified
  saveUninitialized: false, // don't create session until something stored
  secret: 'findmenow@nodejs',
  cookie: { maxAge: 30*60*1000 } // 30 minutes
}));

process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log(err);
});


/****************************
 *    Utilities initial      *
 ****************************/
global.daoLookup = require('./app/DAOLookup.js');
global.ClassForName = require('./app/ClassForName.js');
global.multiparty = require('./app/Multiparty.js');


/****************************
 *    Database initial      *
 ****************************/
 var mysql = require('mysql');
// global.pool = mysql.createPool({
//  connectionLimit : 10,
//  host     : '192.168.33.3',
//  //host     : 'mobilesrv.wisesoft.co.th ',
//  database : 'school',
//  port     : 3306,
//  user     : 'root',
//  password : ''
// });
global.DBManager = require('./app/DBManager.js')(mysql);


/************************
 *   Express Routing    *
 ************************/
var router = require('./app/router');
for(var i=0; i<router.length; i++) {
  var route = router[i];
  // var callback = require(route.callback);

  var url = "/api" + route.url;
  var callback = "./action" + route.callback;

  var requiredSession = true;
  if(route.requiredSession != undefined) requiredSession = route.requiredSession;

  var params = route.params || {};

  if(route.method == 'get') app.get(url, require('./app/ActionManager.js')(callback, requiredSession, params));
  else if(route.method == 'post') app.post(url, require('./app/ActionManager.js')(callback, requiredSession, params));
  else if(route.method == 'put') app.put(url, require('./app/ActionManager.js')(callback, requiredSession, params));
  else if(route.method == 'delete') app.delete(url, require('./app/ActionManager.js')(callback, requiredSession, params));
}

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port " + port);
